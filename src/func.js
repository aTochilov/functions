const getSum = (str1, str2) => {
  if(typeof(str1) != 'string' || typeof(str2) != 'string'
    || str1.match(/\D/g) || str1.match(/\D/g))
        return false
  if(str1.length == 0)
    return str2
  if(str2.length == 0)
    return str1
  let number1 = str1.split('').reverse().map(str => {return Number(str)});
  let number2 = str2.split('').reverse().map(str => {return Number(str)});
  let minLength = str1.length < str2.length? str1.length : str2.length;

  for(let i = 0; i < minLength; i++){
      number1[i] = number1[i] + number2[i]
    if(number1[i] > 9){
      number1[i+1] += 1
      number1[i] = number1[i] - 10
    }
  }
  return number1.reverse().join('');
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0, 
      comms = 0;
  listOfPosts.forEach(post => {
    if(post['author'] == authorName)
      posts++;
    if(post['comments']) 
      post['comments'].forEach(com => {
      if(com['author'] == authorName)
        comms++
    })
  })
  return `Post:${posts},comments:${comms}`
};

const tickets=(people)=> {
  let cashSum = 0
  for(let pers = 0; pers < people.length; pers++){
    if(people[pers] - 25 > cashSum )
      return 'NO'
    else if(people[pers] == 25)
      cashSum += people[pers]
    else
      cashSum += (people[pers] - 25)
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
